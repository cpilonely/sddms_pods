
Pod::Spec.new do |spec|

  spec.name         = "SDDMS_DirectionMapView"
  spec.version      = "0.0.1"
  spec.summary      = "Auto create DirectionMapView with google map"
  spec.description  = "Auto create DirectionMapView with google map"
  spec.homepage     = "https://gitlab.com/cpilonely"

  spec.license      = "MIT"

  spec.author             = { "Phong Nguyen Tang" => "phong.nguyen@seldatinc.com" }

  # spec.platform     = :ios, "5.0"

  spec.source       = { :git => "https://gitlab.com/cpilonely/sddms_pods.git", :tag => "#{spec.version}" }

  spec.source_files  = "SDDMS_DirectionMapView/SDDMS_DirectionMapView/**/*.{swift,h,m,xib}"
  spec.exclude_files = "Classes/Exclude"
  spec.static_framework = true
  spec.dependency "Alamofire"
  spec.dependency "GoogleMaps"

end
