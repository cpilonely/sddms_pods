//
//  GroupLocationModel.swift
//  SDDMS_DirectionMapView
//
//  Created by Phong Nguyen on 2/3/20.
//  Copyright © 2020 Phong Nguyen. All rights reserved.
//

import Foundation
import CoreLocation

public class Location {
    open var latitude: Double?
    open var longitude: Double?
    
    public init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func convertToCLLocationCoordinate2D() -> CLLocationCoordinate2D? {
        var result = CLLocationCoordinate2D()
        guard let lat = latitude, let lng = longitude else { return nil }
        result.latitude = CLLocationDegrees(exactly: lat)!
        result.longitude = CLLocationDegrees(exactly: lng)!
        return result
    }
}
