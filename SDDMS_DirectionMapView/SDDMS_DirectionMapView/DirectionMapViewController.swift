//
//  DirectionMapViewController.swift
//  SDDMS_DirectionMapView
//
//  Created by Phong Nguyen on 2/3/20.
//  Copyright © 2020 Phong Nguyen. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire


public protocol DirectionMapViewControllerDelegate: NSObjectProtocol {
    func didTapMarker(_ location:CLLocationCoordinate2D)
}

open class DirectionMapViewController: UIView {
    public weak var delegate:DirectionMapViewControllerDelegate?
    public var locations:[Location]?
    public var key:String = ""
    public var pathColor:UIColor?
    private let TWO_LOCATION = 2
    private var fromLocation:CLLocationCoordinate2D?
    private var toLocation:CLLocationCoordinate2D?
    var mapView = GMSMapView()
    
    required override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func createMapView() -> UIView {
        mapView.frame = self.frame
        mapView.isMyLocationEnabled = false
        mapView.delegate = self
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let amountLocations = locations?.count ?? 0
        if amountLocations == TWO_LOCATION {
            fromLocation = locations?.first?.convertToCLLocationCoordinate2D()
            toLocation = locations?.last?.convertToCLLocationCoordinate2D()
            drawPath()
        } else if amountLocations > TWO_LOCATION {
            for index in 0..<amountLocations {
                if index+1 > amountLocations {
                    return mapView
                }
                fromLocation = locations?[index].convertToCLLocationCoordinate2D()
                toLocation = locations?[index+1].convertToCLLocationCoordinate2D()
                drawPath()
            }
        }
        return mapView
    }
    
    func drawPath(){
        if (fromLocation == nil || toLocation == nil) {
            return
        } else {
            fetchMapData(fromLocation: self.fromLocation!, toLocation: self.toLocation!)
        }
    }
    
    private func fetchMapData(fromLocation:CLLocationCoordinate2D, toLocation:CLLocationCoordinate2D) {
        
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?" + "origin=\(fromLocation.latitude),\(fromLocation.longitude)&destination=\(toLocation.latitude),\(toLocation.longitude)&" +
        "key=\(key)"
        
        Alamofire.request(directionURL).responseJSON
            { response in
                
                if let JSON = response.result.value {
                    
                    let mapResponse: [String: AnyObject] = JSON as! [String : AnyObject]
                    
                    let routesArray = (mapResponse["routes"] as? Array) ?? []
                    
                    let routes = (routesArray.first as? Dictionary<String, AnyObject>) ?? [:]
                    
                    let overviewPolyline = (routes["overview_polyline"] as? Dictionary<String,AnyObject>) ?? [:]
                    let polypoints = (overviewPolyline["points"] as? String) ?? ""
                    let line  = polypoints
                    
                    self.addPolyLine(encodedString: line)
                    self.showMarkers()
                }
        }
        
    }
    
    private func addPolyLine(encodedString: String) {
        
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 5
        polyline.strokeColor = pathColor ?? .blue
        polyline.map = mapView
        let bounds = GMSCoordinateBounds(path: path!)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
    }
    
    private func showMarkers() {
        guard let listLocations = locations else { return }
        for (index,location) in listLocations.enumerated() {
            let seq = index + 1
            guard let locationCorrdinate2D = location.convertToCLLocationCoordinate2D() else { return }
            showMarker(location: locationCorrdinate2D, sequence: seq)
        }
    }
    
    private func showMarker(location:CLLocationCoordinate2D, sequence: Int) {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: location.latitude,
                                                                longitude: location.longitude))
        let labelOrder = labelMarkerWithText("\(sequence)")
        marker.map = mapView
        marker.iconView = labelOrder
        marker.zIndex = 1
        marker.userData = location
    }
    
    private func labelMarkerWithText(_ text:String) -> UILabel {
        let labelOrder = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        labelOrder.text = text
        labelOrder.textAlignment = .center
        labelOrder.textColor = .white
        labelOrder.backgroundColor = .gray
        labelOrder.layer.cornerRadius = 15.0
        labelOrder.clipsToBounds = true
        
        return labelOrder
    }
}

//MARK: - GMSMapViewDelegate
extension DirectionMapViewController: GMSMapViewDelegate {
    public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let location = marker.userData as? CLLocationCoordinate2D else {
            return false
        }
        self.delegate?.didTapMarker(location)
        return true
    }
}
